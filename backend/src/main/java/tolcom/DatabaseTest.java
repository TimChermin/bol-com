
package tolcom;
/*
import tolcom.model.Product;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseTest {
    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
    private static final Logger logger = Logger.getLogger(DatabaseTest.class.getName());

    public List<Product> readDataBase() {
        List<Product> products = new ArrayList<>();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String strQuery = "SELECT p FROM Product p WHERE p.id IS NOT NULL";
        TypedQuery<Product> tq = entityManager.createQuery(strQuery, Product.class);
        try{
            products = tq.getResultList();
        }
        catch (NoResultException ex){
            logError(ex.toString());
        }
        finally {
            entityManager.close();
        }

        return products;
    }

    public Product getProduct(int id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String query = "SELECT p FROM Product p WHERE p.id = :proID";

        TypedQuery<Product> tq = entityManager.createQuery(query, Product.class);
        tq.setParameter("proID", id);
        Product product = null;
        try{
            product = tq.getSingleResult();
        }
        catch (NoResultException ex){
            logError(ex.toString());
        }
        finally {
            entityManager.close();
        }
        return product;
    }

    public void addProduct(){
        Product product = new Product();
        product.setId(11);
        product.setName("TestProduct");
        product.setDescription("TestDescription");
        product.setPrice("200");
        product.setSrc("beatsEP.jpg");



        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(product);
        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();
    }

    private void log(String msg){
        if (logger.isLoggable(Level.INFO)){
            logger.info(msg);
        }
    }
    private void logError(String msg){
        if (logger.isLoggable(Level.SEVERE)){
            logger.severe(msg);
        }
    }
}*/
