package tolcom.hateoas;

import org.springframework.boot.actuate.endpoint.web.Link;
import tolcom.controller.ProductController;
import tolcom.model.Product;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

public class ProductLinks {
    public void addLinksToProducts(List<Product> products) throws Exception {
        for (Product product : products) {
            int packId = product.getId();
            product.add(linkTo(ProductController.class).withSelfRel());
            product.add(linkTo(ProductController.class).slash("productInfo").slash(packId).withSelfRel());
            /*if (!product.getCategories().isEmpty()) {
                Link productLink = linkTo(methodOn(ProductController.class).getCategoriesOfProduct(packId)).withRel("allCategoriesOfProduct");
                product.add(productLink);
            }*/
        }
    }
}
