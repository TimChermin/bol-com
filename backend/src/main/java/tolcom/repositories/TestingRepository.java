package tolcom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tolcom.model.Product;

@Repository
public interface TestingRepository extends JpaRepository<Product, Integer> {
}
