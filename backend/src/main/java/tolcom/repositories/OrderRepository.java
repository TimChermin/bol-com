package tolcom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tolcom.model.UserOrder;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<UserOrder, Integer> {
    List<UserOrder> findAllByUserId(int userId);
    UserOrder findById(int id);
}
