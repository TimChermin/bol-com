package tolcom.serviceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tolcom.model.user.Privilege;
import tolcom.model.user.Role;
import tolcom.repositories.RoleRepository;
import tolcom.repositories.UserRepository;
import tolcom.security.logins.LoginAttemptService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Collections.emptyList;

@Service("userDetailsService")
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserRepository userRepository;
    private LoginAttemptService loginAttemptService;
    private HttpServletRequest request;

    private static final Logger LOGGER = Logger.getLogger( UserDetailsServiceImpl.class.getName() );

    @Autowired
    public UserDetailsServiceImpl(UserRepository applicationUserRepository, LoginAttemptService loginAttemptService, HttpServletRequest request) {
        this.userRepository = applicationUserRepository;
        this.loginAttemptService = loginAttemptService;
        this.request = request;
    }


    @Override
    public UserDetails loadUserByUsername(String username) {
        String ip = getClientIP();
        if (loginAttemptService.isBlocked(ip)){
            throw new RuntimeException("Blocked");
        }

        tolcom.model.user.User applicationUser = userRepository.findByName(username);
        if (applicationUser == null) {
            LOGGER.log(Level.WARNING, "Wrong login: Username does not exist");
            throw new UsernameNotFoundException(username);
        }


        return new org.springframework.security.core.userdetails.User(
                applicationUser.getName(), applicationUser.getPassword(), applicationUser.isEnabled(), true, true,
                true, getAuthorities(applicationUser.getRoles()));
        //return new User(applicationUser.getName(), applicationUser.getPassword(), emptyList());
    }

    private List<? extends GrantedAuthority> getAuthorities(
            List<Role> roles) {

        return getGrantedAuthorities(getPrivileges(roles));
    }

    private List<String> getPrivileges(List<Role> roles) {

        List<String> privileges = new ArrayList<>();
        List<Privilege> collection = new ArrayList<>();
        for (Role role : roles) {
            collection.addAll(role.getPrivileges());
        }
        for (Privilege item : collection) {
            privileges.add(item.getName());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }

    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null){
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}