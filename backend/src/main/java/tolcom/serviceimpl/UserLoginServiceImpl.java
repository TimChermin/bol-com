package tolcom.serviceimpl;

import org.springframework.stereotype.Service;
import tolcom.model.user.User;
import tolcom.repositories.OrderRepository;
import tolcom.repositories.UserRepository;
import tolcom.services.UserLoginService;

@Service
public class UserLoginServiceImpl implements UserLoginService {

    private UserRepository userRepository;
    
    public void addUser(User applicationUser){
        userRepository.save(applicationUser);
    }

    public User findByName(String userName){
        return userRepository.findByName(userName);
    }
}
