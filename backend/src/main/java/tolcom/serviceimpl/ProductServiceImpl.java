package tolcom.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tolcom.model.Category;
import tolcom.repositories.CategoryRepository;
import tolcom.services.ProductService;
import tolcom.exceptions.ApiRequestException;
import tolcom.model.Product;
import tolcom.repositories.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Product getProduct(int id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> getProducts(int pageNr) {
        int result = (pageNr+1) * 15;
        Page<Product> page = productRepository.findAll(PageRequest.of(0, result, Sort.by(Sort.Direction.ASC, "id")));
        return page.getContent();
    }

    @Override
    public List<Product> getProductByName(String name) {
        return productRepository.findAllByNameLike('%'+name+'%');
    }

    @Override
    public boolean addProduct(Product product) {
        //TODO check if you can remove this
        List<Category> categories = new ArrayList<>();
        for (Category category: product.getCategories()){
            categories.add(categoryRepository.findById(category.getId()));
        }
        product.setCategories(categories);

        if (arePropertiesEmpty(product)){
            throw new ApiRequestException("empty String send");
        }
        productRepository.save(product);
        return true;
    }

    @Override
    public boolean updateProduct(int id, Product product) {
        if (arePropertiesEmpty(product)){
            throw new ApiRequestException("empty String send");
        }
        Product productDb = productRepository.findById(id);
        if (productDb != null){
            productDb.setName(product.getName());
            productDb.setPrice(product.getPrice());
            productDb.setSrc(product.getSrc());
            productDb.setDescription(product.getDescription());
            productDb.setStock(product.getStock());
            productRepository.save(productDb);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteProduct(int id) {
        Product product = productRepository.findById(id);
        if (product != null){
            product.setCategories(new ArrayList<>());
            productRepository.save(product);
            productRepository.delete(product);
            return true;
        }
        return false;
    }

    public static boolean arePropertiesEmpty(Product product){
        return product.getName().equals("") || product.getPrice().equals("")
                || product.getDescription().equals("") || product.getSrc().equals("");
    }
}
