package tolcom.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tolcom.model.OrderDetails;
import tolcom.model.UserOrder;
import tolcom.model.user.User;
import tolcom.repositories.OrderRepository;
import tolcom.repositories.UserRepository;
import tolcom.services.UserOrderService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserOrderOrderServiceImpl implements UserOrderService {

    /*SELECT product.id, product_order.order_id, product.name, product.description, product_order.product_price_on_order, product.image_src FROM product
INNER JOIN product_order ON product.id = product_order.product_id AND product_order.order_id = 2*/
    private UserRepository userRepository;
    private OrderRepository orderRepository;

    @Autowired
    public UserOrderOrderServiceImpl(UserRepository userRepository, OrderRepository orderRepository) {
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
    }

    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
    private static final Logger logger = Logger.getLogger(UserOrderOrderServiceImpl.class.getName());

    public List<OrderDetails> getAllOrders(String username) {
        User user = userRepository.findByName(username);
        EntityManager em = entityManagerFactory.createEntityManager();
        String stQuery = "SELECT NEW OrderDetails (p.name, uo.userId , p.description, p.src, uo.priceOnOrder, uo.dateOnOrder, uo.id, p.id, po.product_price_on_order)  " +
                "FROM Product p " +
                "INNER JOIN ProductOrder po ON p.id = po.productId " +
                "INNER JOIN UserOrder uo ON po.orderId = uo.id AND uo.userId = :userId " +
                "GROUP BY uo.id";

        TypedQuery<OrderDetails> tq = em.createQuery(stQuery, OrderDetails.class);
        tq.setParameter("userId", user.getId());
        List<OrderDetails> resultList = new ArrayList<>();
        try{
            resultList = tq.getResultList();
        }
        catch (NoResultException ex){
            logError(ex.toString());
        }
        finally {
            em.close();
        }
        return resultList;
    }

    public List<OrderDetails> getOrder(int id) {
        System.out.println("-- orders?--");
        EntityManager em = entityManagerFactory.createEntityManager();
        String stQuery = "SELECT NEW OrderDetails (p.name, uo.userId , p.description, p.src, uo.priceOnOrder, uo.dateOnOrder, uo.id, p.id, po.product_price_on_order)  " +
                "FROM Product p " +
                "INNER JOIN ProductOrder po ON p.id = po.productId AND po.orderId = :orderId " +
                "INNER JOIN UserOrder uo ON po.orderId = uo.id";

        TypedQuery<OrderDetails> tq = em.createQuery(stQuery, OrderDetails.class);
        tq.setParameter("orderId", id);
        List<OrderDetails> resultList = new ArrayList<>();
        try{
            resultList = tq.getResultList();
        }
        catch (NoResultException ex){
            logError(ex.toString());
        }
        finally {
            em.close();
        }
        return resultList;
    }

    @Override
    public void addOrder(UserOrder userOrder) {
        orderRepository.save(userOrder);
    }

    public void addOrder(){
        UserOrder product = new UserOrder();
        /*product.setId(11);
        product.setName("TestProduct");
        product.setDescription("TestDescription");
        product.setPrice("200");
        product.setSrc("beatsEP.jpg");*/



        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(product);
        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();
    }

    private void log(String msg){
        if (logger.isLoggable(Level.INFO)){
            logger.info(msg);
        }
    }
    private void logError(String msg){
        if (logger.isLoggable(Level.SEVERE)){
            logger.severe(msg);
        }
    }
}
