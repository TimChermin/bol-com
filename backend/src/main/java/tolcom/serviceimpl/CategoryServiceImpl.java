package tolcom.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tolcom.exceptions.ApiRequestException;
import tolcom.model.Category;
import tolcom.model.Product;
import tolcom.repositories.CategoryRepository;
import tolcom.services.CategoryService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Category getCategory(int id) {
        return categoryRepository.findById(id);
    }

    @Override
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public List<Category> getCategoryByName(String name) {
        return categoryRepository.findAllByName(name);
    }

    @Override
    public boolean addCategory(Category category) {
        categoryRepository.save(category);
        return true;
    }

    @Override
    public boolean updateCategory(int id, Category category) {
        if (arePropertiesEmpty(category)){
            throw new ApiRequestException("empty String send");
        }
        Category categoryDB = categoryRepository.findById(id);
        if (categoryDB != null){
            categoryDB.setName(category.getName());
            categoryDB.setDescription(category.getDescription());
            categoryRepository.save(categoryDB);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteCategory(int id) {
        Category category = categoryRepository.findById(id);
        if (category != null){
            categoryRepository.delete(category);
            return true;
        }
        return false;
    }

    public static boolean arePropertiesEmpty(Category category){
        return category.getName().equals("") || category.getDescription().equals("");
    }
}
