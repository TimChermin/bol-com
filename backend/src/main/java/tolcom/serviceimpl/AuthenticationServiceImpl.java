package tolcom.serviceimpl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import tolcom.services.AuthenticationService;

import static tolcom.security.SecurityConstants.SECRET;
import static tolcom.security.SecurityConstants.TOKEN_PREFIX;

public class AuthenticationServiceImpl implements AuthenticationService {
    @Override
    public boolean validateAdminToken(String jwt) {
        return false;
    }

    @Override
    public boolean validateUserToken(String jwt, String userName) {
        String tokenSubject = getTokenSubject(jwt);
        return userName != null && userName.equals(tokenSubject);
    }

    @Override
    public String getTokenSubject(String jwt){
        if(jwt == null) return null;

        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(jwt.replace(TOKEN_PREFIX, ""))
                .getSubject();
    }
}
