package tolcom.security;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityImpl implements GrantedAuthority {
    private String privilege;

    public GrantedAuthorityImpl (String privilege){
        this.privilege = privilege;
    }

    @Override
    public String getAuthority() {
        return privilege;
    }
}
