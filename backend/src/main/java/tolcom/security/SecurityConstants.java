package tolcom.security;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String PRODUCTS = "/products";
    public static final String PRODUCT_INFO = "/products/productInfo/{id}";
    public static final String CAROUSEL = "/carouselItems";
    public static final String ADD_PRODUCT = "/addProduct";
    public static final String EDIT_PRODUCT = "/products/editProduct/{productId}";
    public static final String DELETE_PRODUCT = "/products/deleteProduct/{productId}";
    public static final String GET_USER_ROLE = "/users/{userName}/role";
    public static final String GET_USER_ORDERS = "/users/{userName}/orders";
}
