package tolcom.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;

@Entity
@Table(name = "productorder")
@EntityListeners(AuditingEntityListener.class)
public class ProductOrder extends RepresentationModel<ProductOrder> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name="id")
    private int id;

    @Column(name="order_id")
    private int orderId;

    @Column(name="product_id")
    private int productId;

    @Column(name="product_price_on_order")
    private int product_price_on_order;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProduct_price_on_order() {
        return product_price_on_order;
    }

    public void setProduct_price_on_order(int productPriceOnOrder) {
        this.product_price_on_order = productPriceOnOrder;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
