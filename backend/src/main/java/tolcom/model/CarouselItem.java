package tolcom.model;

public class CarouselItem {
    private String name;
    private String routingLocation;
    private String src;
    private int productId;


    public CarouselItem(String name, String routingLocation, String src, int productId) {
        this.name = name;
        this.routingLocation = routingLocation;
        this.src = src;
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoutingLocation(String routingLocation) {
        this.routingLocation = routingLocation;
    }

    public String getRoutingLocation() {
        return routingLocation;
    }

    public String getSrc() {
        return src;
    }
}
