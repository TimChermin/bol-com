package tolcom.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "userorder")
@EntityListeners(AuditingEntityListener.class)
public class UserOrder extends RepresentationModel<UserOrder> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name="id")
    private int id;

    @Column(name="user_id")
    private int userId;

    @Column(name="price_on_order")
    private int priceOnOrder;

    @Column(name="date_on_order")
    private Date dateOnOrder;

    /*@ManyToMany
    @JoinTable(name = "product_order", joinColumns = @JoinColumn(name="userorder_id", unique = false),
            inverseJoinColumns = @JoinColumn(name = "product_id", unique = false))
    private List<Product> products;*/

    /*@ManyToMany
    @JoinTable(name = "product_order", joinColumns = @JoinColumn(name="order_id", unique = false),
            inverseJoinColumns = @JoinColumn(name = "product_id", unique = false))
    private List<ProductOrder> productOrder;
*/
    public UserOrder(){

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOnOrder() {
        return dateOnOrder;
    }

    public void setDateOnOrder(Date dateOnOrder) {
        this.dateOnOrder = dateOnOrder;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setPriceOnOrder(int priceOnOrder) {
        this.priceOnOrder = priceOnOrder;
    }

    public int getPriceOnOrder() {
        return priceOnOrder;
    }

    /*public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }*/

    /*public List<ProductOrder> getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(List<ProductOrder> productOrder) {
        this.productOrder = productOrder;
    }*/
}
