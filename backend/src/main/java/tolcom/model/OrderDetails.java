package tolcom.model;

import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.util.Date;

@Entity
public class OrderDetails extends RepresentationModel<OrderDetails> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String name;

    private String src;

    private String description;

    private int orderId;

    private int productId;

    private int productPriceOnOrder;

    private int userId;

    private int priceOnOrder;

    private Date dateOnOrder;

    public OrderDetails(){

    }

    public OrderDetails(String name, int userId, String description, String src, int priceOnOrder, Date dateOnOrder, int orderId, int productId, int productPriceOnOrder){
        this.name = name;
        this.userId = userId;
        this.description = description;
        this.src = src;
        this.priceOnOrder = priceOnOrder;
        this.dateOnOrder = dateOnOrder;
        this.orderId = orderId;
        this.productId = productId;
        this.productPriceOnOrder = productPriceOnOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductPriceOnOrder() {
        return productPriceOnOrder;
    }

    public void setProductPriceOnOrder(int productPriceOnOrder) {
        this.productPriceOnOrder = productPriceOnOrder;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPriceOnOrder() {
        return priceOnOrder;
    }

    public void setPriceOnOrder(int priceOnOrder) {
        this.priceOnOrder = priceOnOrder;
    }

    public Date getDateOnOrder() {
        return dateOnOrder;
    }

    public void setDateOnOrder(Date dateOnOrder) {
        this.dateOnOrder = dateOnOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
