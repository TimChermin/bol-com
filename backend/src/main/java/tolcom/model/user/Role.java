package tolcom.model.user;
import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Role {
    //this represents the high-level roles of the user in the system; each role will have a set of low-level privileges

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "roles_privileges",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "privilege_id", referencedColumnName = "id"))
    private List<Privilege> privileges;

    public Role(){

    }

    public Role(String name){
        this.name = name;
    }


    public void setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public String getName() {
        return name;
    }
}
