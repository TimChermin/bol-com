package tolcom.model.user;
import javax.persistence.*;
import java.util.Set;

@Entity
public class Privilege {
    //represents a low-level, granular privilege/authority in the system
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Set<Role> roles;

    public Privilege(){

    }

    public Privilege(String name){
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
