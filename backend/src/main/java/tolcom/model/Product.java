package tolcom.model;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.RepresentationModel;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@FilterDef(name = "nameFilter", defaultCondition = "name == filteredName", parameters = { @ParamDef(name = "filteredName", type = "String") })
@Entity
@Table(name = "product")
@EntityListeners(AuditingEntityListener.class)
public class Product extends RepresentationModel<Product> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="price")
    private String price;

    @Column(name="image_src")
    private String src;

    @Column(name="description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "product_category", joinColumns = @JoinColumn(name="product_id"),
    inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories = new ArrayList<>();

    @Column(name="stock")
    private int stock;


    private String nameFilter;

    public Product(String name, String price, String description, String src, List<Category> categories, int stock){
        this.name = name;
        this.price = price;
        this.description = description;
        this.src = src;
        this.categories = categories;
        this.stock = stock;

    }

    public Product(){

    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Category> getCategories() {
        return categories;
    }
    public void addCategory(Category category){
        categories.add(category);
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public String getNameFilter() {
        return nameFilter;
    }


}


