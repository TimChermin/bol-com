package tolcom.services;

import tolcom.model.user.User;


public interface UserLoginService {
    User findByName(String name);
    void addUser(User applicationUser);
}
