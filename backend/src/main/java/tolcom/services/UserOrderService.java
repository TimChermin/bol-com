package tolcom.services;

import tolcom.model.OrderDetails;
import tolcom.model.UserOrder;

import java.util.List;

public interface UserOrderService {
    List<OrderDetails> getAllOrders(String userName);
    List<OrderDetails> getOrder(int id);
    void addOrder(UserOrder userOrder);
}
