package tolcom.services;

import tolcom.model.Product;
import java.util.List;

public interface ProductService {
    Product getProduct(int id);
    List<Product> getProducts(int pageNr);
    List<Product> getProductByName(String name);
    boolean addProduct(Product product);
    boolean updateProduct(int id, Product product);
    boolean deleteProduct(int id);
}
