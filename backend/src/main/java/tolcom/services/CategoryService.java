package tolcom.services;

import tolcom.model.Category;

import java.util.List;

public interface CategoryService {
    Category getCategory(int id);
    List<Category> getCategories();
    List<Category> getCategoryByName(String name);
    boolean addCategory(Category category);
    boolean updateCategory(int id, Category category);
    boolean deleteCategory(int id);
}
