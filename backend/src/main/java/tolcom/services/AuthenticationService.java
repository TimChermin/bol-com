package tolcom.services;

public interface AuthenticationService {

    boolean validateAdminToken(String jwt);
    boolean validateUserToken(String jwt, String userName);
    String getTokenSubject(String jwt);
}
