package tolcom.controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tolcom.model.CarouselItem;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins =  {"http://localhost:8080", "http://192.168.2.40:8080", "https://bol-com.now.sh/", "https://bol-com-git-deploy.timchermin.now.sh/", "https://bol-com.timchermin.now.sh/"})
@RestController
public class CarouselController {

    @RequestMapping("/carouselItems")
    public List<CarouselItem> index(){
        List<CarouselItem> carousel = new ArrayList<>();
        carousel.add(new CarouselItem("Slide 1", "productinformation", "banner1.jpg", 1));
        carousel.add(new CarouselItem("Slide 2", "productinformation", "banner3.jpg", 2));
        carousel.add(new CarouselItem("Slide 3", "productinformation", "banner1.jpg", 3));
        carousel.add(new CarouselItem("Slide 4", "productinformation", "banner3.jpg", 4));
        carousel.add(new CarouselItem("Slide 5", "productinformation", "banner1.jpg", 5));
        carousel.add(new CarouselItem("Slide 6", "productinformation", "banner3.jpg", 6));
        carousel.add(new CarouselItem("Slide 7", "productinformation", "banner1.jpg",7 ));
        carousel.add(new CarouselItem("Slide 8", "productinformation", "banner3.jpg", 8));
        carousel.add(new CarouselItem("Slide 9", "productinformation", "banner1.jpg", 9));

        return carousel;
    }
}