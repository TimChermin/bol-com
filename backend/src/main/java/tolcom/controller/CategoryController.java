package tolcom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tolcom.exceptions.ApiRequestException;
import tolcom.model.Category;
import tolcom.services.CategoryService;

import java.util.List;

@CrossOrigin(origins =  {"http://localhost:8080", "http://localhost:5000", "http://192.168.2.40:8080", "https://bol-com.now.sh/", "https://bol-com-git-deploy.timchermin.now.sh/", "https://bol-com.timchermin.now.sh/"})
@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;
    private String idDoesNotExist = "Category id does not exist";
    private String categoriesNotFound = "Categories not found";

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<Category> getAllCategories() throws Exception {
        List<Category> categories = categoryService.getCategories();
        if (categories != null) {
            return categories;
        }
        throw new ApiRequestException(categoriesNotFound);
    }

}
