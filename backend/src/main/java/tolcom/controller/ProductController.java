package tolcom.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tolcom.services.ProductService;
import tolcom.exceptions.ApiRequestException;
import tolcom.hateoas.ProductLinks;
import tolcom.model.Product;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@CrossOrigin(origins =  {"http://localhost:8080", "http://localhost:5000", "http://192.168.2.40:8080", "https://bol-com.now.sh/", "https://bol-com-git-deploy.timchermin.now.sh/", "https://bol-com.timchermin.now.sh/"})
@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;
    private ProductLinks productLinks;
    private String idDoesNotExist = "Product id does not exist";
    private String productsNotFound = "Products not found";

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
        this.productLinks = new ProductLinks();
    }

    @GetMapping ("/productName/{name}")
    public List<Product> getProductsByName(@PathVariable String name) throws Exception {
        List<Product> products = productService.getProductByName(name);
        if (products != null){
            return products;
        }
        throw new ApiRequestException(productsNotFound);
    }

    @GetMapping (value = "/nameLike", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAllProductsByName(@RequestParam String name) throws Exception {
        List<Product> products = productService.getProductByName(name);
        if (products != null){
            return products;
        }
        throw new ApiRequestException(productsNotFound);
    }

    @GetMapping
    public List<Product> getAllProducts(@RequestParam int pageNr) throws Exception {
        List<Product> products = productService.getProducts(pageNr);
        if (products != null){
            return products;
        }
        throw new ApiRequestException(productsNotFound);


        /*int result = (pageNr+1) * 15;
        Page<Product> page = productService.findAll(PageRequest.of(0, result, Sort.by(Sort.Direction.ASC, "id")));
        List<Product> products = page.getContent();
        productLinks.addLinksToProducts(products);
        //Link link = linkTo(ProductController.class).withSelfRel();
        return products;*/
    }

    @GetMapping("/productInfo/{productId}")
    public Product getProductById(@PathVariable int productId ) throws Exception {
        Product product = productService.getProduct(productId);
        if (product == null){
            throw new ApiRequestException(idDoesNotExist);
        }
        return product;
    }

    @PostMapping("/editProduct/{productId}")
    public HttpStatus updateProduct(@PathVariable int productId, @RequestBody Product product ) throws Exception {
        if(productService.updateProduct(productId, product)) {
            return HttpStatus.OK;
        }
        throw new ApiRequestException(idDoesNotExist);

    }

    @DeleteMapping("/deleteProduct/{productId}")
    public HttpStatus deleteProduct(@PathVariable int productId) throws Exception {
        if(productService.deleteProduct(productId)) {
            return HttpStatus.OK;
        }
        throw new ApiRequestException(idDoesNotExist);
    }

    @PostMapping("/add-product")
    public HttpStatus addProduct(@RequestBody Product product) throws Exception {
        if(productService.addProduct(product)) {
            return HttpStatus.OK;
        }
        return HttpStatus.BAD_REQUEST;
    }


    private Product getProductData(JSONObject jsonObject) {
        try{
            Product product = new Product();
            product.setName(jsonObject.get("name").toString());
            product.setPrice(jsonObject.get("price").toString());
            product.setSrc(jsonObject.get("src").toString());
            product.setDescription(jsonObject.get("description").toString());
            return product;
        }
        catch (Exception ex){
            throw new ApiRequestException(ex.toString());
        }
    }
}
