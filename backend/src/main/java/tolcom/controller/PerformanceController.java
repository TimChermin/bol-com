package tolcom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tolcom.exceptions.ApiRequestException;
import tolcom.model.Product;
import tolcom.serviceimpl.UserDetailsServiceImpl;
import tolcom.services.ProductService;

import java.util.*;
import java.util.logging.Logger;

@CrossOrigin(origins = {"http://localhost:8080", "https://bol-com.now.sh/", "https://bol-com-git-deploy.timchermin.now.sh/", "https://bol-com.timchermin.now.sh/"})
@RestController
@RequestMapping(value = "/performance")
public class PerformanceController {
    private ProductService productService;
    private List<Product> products;
    private static final Logger LOGGER = Logger.getLogger( PerformanceController.class.getName() );

    @Autowired
    public PerformanceController(ProductService productService) {
        this.productService = productService;
        products = new ArrayList<>();
    }

    @GetMapping
    public List<Product> getAllProducts(@RequestParam int pageNr) throws Exception {
        long startTime = System.currentTimeMillis();

        products = getProducts(pageNr);
        LOGGER.info(((new Date()).getTime() - startTime) + " Milliseconds" );
        return products;
    }

    private List<Product> getProducts(int pageNr){
        List<Product> productsReturn = productService.getProducts(pageNr);
        if (productsReturn != null){
            return productsReturn;
        }
        throw new ApiRequestException("Products not found");
    }

}
