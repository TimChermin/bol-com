package tolcom.controller;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import tolcom.exceptions.ApiRequestException;
import tolcom.model.OrderDetails;
import tolcom.model.UserOrder;
import tolcom.model.Product;
import tolcom.model.user.Role;
import tolcom.model.user.User;
import tolcom.repositories.ProductRepository;
import tolcom.repositories.RoleRepository;
import tolcom.serviceimpl.AuthenticationServiceImpl;
import tolcom.services.AuthenticationService;
import tolcom.services.UserLoginService;
import tolcom.services.UserOrderService;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:8080", "https://bol-com.now.sh/", "https://bol-com-git-deploy.timchermin.now.sh/", "https://bol-com.timchermin.now.sh/"})
@RestController
@RequestMapping(value = "/users")
public class UserController {

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private RoleRepository roleRepository;
    private AuthenticationService authenticationService;
    private UserOrderService userOrderService;
    private UserLoginService userLoginService;

    @Autowired
    public UserController(BCryptPasswordEncoder bCryptPasswordEncoder, UserOrderService userOrderService, UserLoginService userLoginService, RoleRepository roleRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authenticationService = new AuthenticationServiceImpl();
        this.userOrderService = userOrderService;
        this.userLoginService = userLoginService;
        this.roleRepository = roleRepository;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody User applicationUser) {
        applicationUser.setPassword(bCryptPasswordEncoder.encode(applicationUser.getPassword()));
        applicationUser.setRoles(roleRepository.findAllByName("ROLE_USER"));
        applicationUser.setEnabled(true);
        userLoginService.addUser(applicationUser);
    }

    @GetMapping("/{userName}/role")
    public Role getRoles(@PathVariable String userName, @RequestHeader("Authorization") String jwt) throws JSONException {
        //JSONObject body = new JSONObject(headerString);
        //System.out.println(body);
        //String jwt = body.getString("userToken");

        if (jwt == null || jwt.isEmpty() || !authenticationService.validateUserToken(jwt, userName)){
            throw new ApiRequestException("userName wrong or JWT not send");
        }
        User user = userLoginService.findByName(userName);

        if (user != null){
            return user.getRoles().get(0);
        }
        throw new ApiRequestException("nameDoesNotExist");
    }

    @PostMapping("/{userName}/orders/add")
    public HttpStatus addOrder(@PathVariable String userName, @RequestHeader("Authorization") String jwt, @RequestBody UserOrder userOrder) {
        validateRequest(jwt, userName);
        User user = userLoginService.findByName(userName);
        UserOrder userOrderToDB = new UserOrder();
        userOrderToDB.setUserId(user.getId());
        if (user.getId() == 0 || userOrderToDB.getUserId() == 0){
            return HttpStatus.BAD_REQUEST;
        }

        userOrderToDB.setPriceOnOrder(userOrder.getPriceOnOrder());
        //userOrderToDB.setProducts(getProducts(userOrder.getProducts()));
        userOrderToDB.setDateOnOrder(userOrder.getDateOnOrder());
        userOrderService.addOrder(userOrderToDB);
        return HttpStatus.OK;
    }

    /*private List<Product> getProducts(List<Product> products){
        List<Product> productsFromDB = new ArrayList<>();
        for (Product product: products){
            productsFromDB.add(productRepository.findById(product.getId()));
        }
        return productsFromDB;
    }*/

    @GetMapping("/{userName}/orders")
    public List<OrderDetails> getAllOrders(@PathVariable String userName, @RequestHeader("Authorization") String jwt ) {
        validateRequest(jwt, userName);
        return userOrderService.getAllOrders(userName);
    }

    @GetMapping("/{userName}/orders/{orderId}")
    public List<OrderDetails> getAllOrders(@PathVariable String userName, @PathVariable String orderId, @RequestHeader("Authorization") String jwt ) {
        //validateRequest(jwt, userName);
        return userOrderService.getOrder(Integer.parseInt(orderId));
    }
    private void validateRequest(String jwt, String userName){
        if (jwt == null || jwt.isEmpty() || !authenticationService.validateUserToken(jwt, userName)){
            throw new ApiRequestException("userName wrong or JWT not send");
        }
    }
}
