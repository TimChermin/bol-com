package tolcom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import tolcom.model.Category;
import tolcom.repositories.CategoryRepository;
import tolcom.serviceimpl.CategoryServiceImpl;
import tolcom.services.CategoryService;
import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CategoryTests {

    @Resource
    private CategoryRepository categoryRepository;

    private CategoryService categoryService;
    private Category category;
    private Category category2;
    private static boolean addedToDB;



    @BeforeEach
    public void before(){
        this.categoryService = new CategoryServiceImpl(categoryRepository);
        category = new Category("Computers", "This is about computers");
        category2 = new Category("Laptops", "This is about laptops");
        if (!addedToDB){
            categoryService.addCategory(category);
            categoryService.addCategory(category2);
            addedToDB = true;
        }
    }


    @Test
    void getAllCategories_AssertReturnsAllCategories() {
        for (int i = 0; i < 17; i++) {
            Category category3 = new Category("Books", "This is about books");
            categoryService.addCategory(category3);
        }

        List<Category> categoryList = categoryService.getCategories();
        assertEquals(category, categoryList.get(0));
        assertEquals(20, categoryList.size());
    }

    @Test
    void getCategoryById_AssertReturnsCategory() {
        Category category3 = categoryService.getCategory(1);
        List<Category> categoryList = categoryService.getCategories();
        assertEquals(category.getName(), category3.getName());
    }

    @Test
    void getCategoryByName_AssertReturnsCategory() {
        List<Category> category3 = categoryService.getCategoryByName(category.getName());
        assertEquals(category, category3.get(0));
    }

    @Test
    void deleteCategory_AssertReturnsCategory() {
        Category newCategory = new Category("DELETE", "Updating");
        categoryService.addCategory(newCategory);
        List<Category> categoryToDelete = categoryService.getCategoryByName("DELETE");
        boolean result = categoryService.deleteCategory(categoryToDelete.get(0).getId());
        Category empty = categoryService.getCategory(categoryToDelete.get(0).getId());

        assertEquals(newCategory, categoryToDelete.get(0));
        assertTrue(result);
        assertNull(empty);
    }

    @Test
    void updateCategory_AssertReturnsUpdatedCategory() {
        Category newCategory = new Category("Update", "Updating");

        categoryService.addCategory(newCategory);
        List<Category> categoryToUpdate = categoryService.getCategoryByName("Update");
        categoryToUpdate.get(0).setName("new Name");
        boolean result = categoryService.updateCategory(categoryToUpdate.get(0).getId(), categoryToUpdate.get(0));
        Category updatedCategory = categoryService.getCategory(categoryToUpdate.get(0).getId());

        assertTrue(result);
        assertEquals("new Name", updatedCategory.getName());
    }
}
