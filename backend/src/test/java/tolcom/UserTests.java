package tolcom;

import org.springframework.boot.test.context.SpringBootTest;
import tolcom.model.Category;
import tolcom.model.Product;
import tolcom.serviceimpl.UserOrderOrderServiceImpl;
import tolcom.services.ProductService;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class UserTests {

    @Resource
    private UserOrderOrderServiceImpl userService;


    private ProductService productService;
    private Product product;
    private Product product2;
    private List<Category> categoryList;
    private static boolean addedToDB;



   /* @BeforeEach
    public void before(){
        this.productService = new ProductServiceImpl(productRepository, categoryRepository);
        categoryList = new ArrayList<>();
        product = new Product("PC", "100", "description", "img.png", categoryList, 10);
        product2 = new Product("PC2", "10011", "descriptionfewa", "fewimg.png", categoryList, 102);
        if (!addedToDB){
            productService.addProduct(product);
            productService.addProduct(product2);
            addedToDB = true;
        }
    }



    @Test
    void getProductsByPage_AssertReturnsOnly15Products() {
        for (int i = 0; i < 18; i++) {
            Product productToAdd = new Product("PC3", "100", "description", "img.png", categoryList, 10);
            productService.addProduct(productToAdd);
        }

        List<Product> productList = productService.getProducts(0);
        assertEquals(product, productList.get(0));
        assertEquals(productList.size(), 15);
    }

    @Test
    void getProductById_AssertReturnsProduct() {
        Product product3 = productService.getProduct(1);
        assertEquals(product, product3);
    }

    @Test
    void getProductByName_AssertReturnsProduct() {
        List<Product> product3 = productService.getProductByName(product.getName());
        assertEquals(product, product3.get(0));
    }

    @Test
    void deleteProduct_AssertReturnsProduct() {
        Product product3 = productService.getProduct(1);
        boolean result = productService.deleteProduct(1);
        Product empty = productService.getProduct(1);

        assertEquals(product, product3);
        assertTrue(result);
        assertNull(empty);
    }

    @Test
    void updateProduct_AssertReturnsProduct() {
        Product product3 = productService.getProduct(1);
        product3.setName("new Name");
        boolean result = productService.updateProduct(1, product3);
        Product updatedProduct = productService.getProduct(1);

        assertTrue(result);
        assertEquals("new Name", updatedProduct.getName());
    }*/
}
