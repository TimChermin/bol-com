import { shallowMount, createLocalVue } from '@vue/test-utils';
import Buefy from 'buefy';
import Navbar from "../../../src/components/Navbar";

const localVue = createLocalVue();
localVue.use(Buefy);
let wrapper;

describe('Navbar logged in', () => {
    beforeEach(() => {
        wrapper = shallowMount(Navbar, {
            localVue,
            stubs: ['router-link']
        });
        wrapper.setData({
            loggedIn: true,
            productsNavBar:
                [{
                    "name": "Monitor",
                    "price": "100",
                    "description": "Description",
                    "src": "PhilipsCurvoMonitor.jpg"
                },]
        });
    });

    it("Shows logout button when logged in", () => {
        expect(wrapper.find("#BtnsLoggedInOrOut").text()).toContain("Logout");
    });

    it("Does not show login button when logged in", () => {
        expect(wrapper.find("#BtnsLoggedInOrOut").text()).not.toContain("Login");
    });

    it("Does not show register button when logged in", () => {
        expect(wrapper.find("#BtnsLoggedInOrOut").text()).not.toContain("Sign up");
    });
});


describe('Navbar logged out', () => {
    beforeEach(() => {
        wrapper = shallowMount(Navbar, {
            localVue,
            stubs: ['router-link']
        });
        wrapper.setData({
            loggedIn: false,
            productsNavBar:
                [{
                    "name": "Monitor",
                    "price": "100",
                    "description": "Description",
                    "src": "PhilipsCurvoMonitor.jpg"
                },]
        });
    });

    it("Shows logout button when logged in", () => {
        expect(wrapper.find("#BtnsLoggedInOrOut").text()).toContain("Sign up Log in");
    });

    it("Does not show login button when logged in", () => {
        expect(wrapper.find("#BtnsLoggedInOrOut").text()).not.toContain("Logout");
    });
});