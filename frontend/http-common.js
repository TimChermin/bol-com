import axios from 'axios';

/*export const AXIOS = axios.create({
    baseURL: `http://localhost:5000`,
    headers: {
        'Access-Control-Allow-Origin': 'http://localhost:8080',
        'Access-Control-Expose-Headers': 'Authorization'
    }
});*/

export const AXIOS = axios.create({
    baseURL: `https://tolcom.herokuapp.com`,
    headers: {
        'Access-Control-Allow-Origin': 'https://bol-com.now.sh/',
        'Access-Control-Expose-Headers': 'Authorization',
        'Content-Security-Policy': 'upgrade-insecure-requests'
    }
});
