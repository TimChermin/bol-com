import Vue from 'vue';
import App from './App.vue';
import { ValidationProvider } from 'vee-validate';
import { ValidationObserver } from 'vee-validate';
import router from './router/router';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';

Vue.use(Buefy, {
  defaultIconPack: 'fa'
});


Vue.config.productionTip = false;
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

new Vue({
  router,
  el: '#app',
  components: {
    ValidationProvider,
    ValidationObserver,
  },
  data: () => ({
    value: ''
  }),

  render: h => h(App),
}).$mount('#app');


