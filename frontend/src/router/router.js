import Vue from 'vue'
import Router from 'vue-router'
import Products from '../components/Products.vue'
import About from '../components/About.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import ProductInformation from '../components/ProductInformation.vue'
import Checkout from '../components/Checkout.vue'
import AllProducts from "../components/AllProducts"
import AddProduct from "../components/AddProduct"
import EditProduct from "../components/EditProduct"
import Unauthorized from "../components/Unauthorized"
import apiService from "../services/apiService"
import Orders from "../components/UserOrders"
import Order from "../components/Order"

Vue.use(Router);

export const router = new Router({
    routes: [
        {
            path: '/',
            name: 'products',
            component: Products
        },
        {
            path: '/products/search/:productName',
            name: 'productsSearch',
            component: Products
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/products/productInfo/:productId',
            name: 'productInformation',
            component: ProductInformation
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: Checkout
        },
        {
            path: '/order/:orderId',
            name: 'specificOrder',
            component: Order
        },
        {
            path: '/orders',
            name: 'orders',
            component: Orders
        },
        {
            path: '/allProducts',
            name: 'allProducts',
            component: AllProducts,
            meta: { authorize: ["ROLE_ADMIN"] }
        },
        {
            path: '/addProduct',
            name: 'addProduct',
            component: AddProduct,
            meta: { authorize: ["ROLE_ADMIN"] }
        },
        {
            path: '/editProduct/:productId',
            name: 'editProduct',
            component: EditProduct,
            meta: { authorize: ["ROLE_ADMIN"] }
        },
        {
            path: '/unauthorized',
            name: 'unauthorized',
            component: Unauthorized,
        }
    ],
    Router: Router
});
export default router

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const { authorize } = to.meta;
    if (authorize) {
        const currentUser = JSON.parse(localStorage.getItem("user"));
        if (!currentUser) {
            // not logged in so redirect to login page with the return url
            return next({ path: '/login', query: { returnUrl: to.path } });
        }

        // check if route is restricted by role
        if (authorize.length /*&& !authorize.includes(currentUser.role)*/) {
            const userToken = JSON.parse(localStorage.getItem("userToken"));
            return makeApiCall(currentUser, userToken, authorize, next);
            // role not authorised so redirect
        }
        else {
            return next({ path: '/unauthorized' });
        }
    }
    next();
});


async function makeApiCall(currentUser, userToken, authorize, next ){
    apiService.getUserRole(currentUser, userToken).then(result => {
        if (authorize.includes(result.data.name)){
            return next();
        }
        return next({ path: '/unauthorized' });
    }) .catch(e => {
        console.log(e);
        return next({ path: '/unauthorized' });
    });
}