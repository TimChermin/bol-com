import { AXIOS } from "../../http-common";
import authHeader from "../authHeader";

export default {
    addProduct(data) {
        return postRequest(`products/add-product`, data);
    },

    editProduct(productId, data){
        return postRequest(`products/editProduct/${productId}`, data)
    },

    deleteProduct(productId){
        return deleteRequest(`products/deleteProduct/${productId}`)
    },

    getAllCategories(){
        return getRequest('categories');
    },


    getUserInfo(userName) {
        return getRequest(`users/${userName}`);
    },

    getUserRole(userName, data) {
        return getRequestWithData(`users/${userName}/role`, data);
    },

    getOrders(userName) {
        return getRequest(`users/${userName}/orders`);
    },

    getOrder(userName, orderId) {
        return getRequest(`users/${userName}/orders/${orderId}`);
    },

    addOrder(userName, data){
        return postRequest(`users/${userName}/orders/add`, data);
    }

}

const getRequest = (url) => {

    return AXIOS.get(`${url}`, {
        method: 'GET',
        mode: 'cors',
        headers: authHeader(),
    }).then((response) => {

        return response;
    })
        .catch(e => {
            console.log(e);
        });
};

const getRequestWithData = (url, data) => {

    return AXIOS.get(`${url}`, {
        method: 'GET',
        headers: authHeader(),
        data: data,
    }).then((response) => {

        return response;
    })
        .catch(e => {
            console.log(e);
        });
};

const postRequest = (url, data) => {
    return AXIOS.post(url, data,{
        headers: authHeader(),
    })
};

const deleteRequest = (url) => {
    return AXIOS.delete(url, {
        headers: authHeader(),
        'Access-Control-Allow-Methods': 'DELETE'
    })
};