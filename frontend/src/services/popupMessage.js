import { NotificationProgrammatic as notification } from 'buefy'

export default function showPopup(message, type){
    notification.open({
        duration: 5000,
        message: message,
        position: "is-bottom-right",
        type: type,
        hasIcon: true,
        closable: false
    });
}