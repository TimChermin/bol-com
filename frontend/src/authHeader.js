export default function authHeader() {
    let userToken = JSON.parse(localStorage.getItem('userToken'));

    if (userToken) {
        return { Authorization: userToken };
    } else {
        console.log("No token found");
        return null;
    }
}